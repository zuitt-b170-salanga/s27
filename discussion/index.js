/* s27-index.js */

// const http = require ('http');

// const port = 4000;

// const server = http.createServer((req, res)=>{ 
// 	http method of the incoming requests can be accessed via "req.method"
// 		GET method - retrieving/reading info; default method
// 	if (req.url === "/items"&&req.method ==="GET"){
// 		res.writeHead(200, {"Content-Type": "text/plain"});
//  		res.end("Data retrieved from the database");
// 	}

// })

// server.listen(port);

//console.log(`Server is running at localhost:${port}`);


//=========================================
const http = require("http");

const port = 4000;

const server = http.createServer( ( req, res ) =>{

	// http method of the incoming requests can be accessed via "req.method"
		//GET method - retrieving/reading info; default method
	if ( req.url === "/items" && req.method === "GET" ) {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Data retrieved from the database");
	}

/*
	create a "items" url w/ POST method
	the response shd be "Data to be sent to the database" w/ 200 as status code & plain
	txt as the content type
*/

/*
	POST,PUT,DELETE operation do not work in the browser unlike the Get method, with this, 
	Postman solves the problem by simulating a frontend for the devs to test their codes
*/

	if ( req.url === "/items" && req.method === "POST" ) {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Data to be sent to the database");
	}


	/*
		put, delete
	*/

	if ( req.url === "/items" && req.method === "PUT" ) {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Update resources");
	}
	if ( req.url === "/items" && req.method === "DELETE" ) {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Delete resources");
	}

} );

server.listen(port);

console.log(`Server is running at localhost: ${port}`);

//=======================================================
// 2)Terminal
// marizzehaideesalanga@Marizzes-MacBook-Pro discussion % node index.js
// Server is running at localhost: 4000

// 3)Browser: (GET)
// localhost:4000/items
// Data retrieved from the database

// 4)Postman
// Add Request ==> Get Request
// GET
// http://localhost:4000/items
// SEND
// Data retrieved from the database

// Add Request ==> Post Request
// POST
// http://localhost:4000/items
// SEND
// Data to be sent to the database

// Add Request ==> Put Request
// PUT
// http://localhost:4000/items
// SEND
// Update resources

// Add Request ==> Delete Request
// DELETE
// http://localhost:4000/items
// SEND
// Delete resources
