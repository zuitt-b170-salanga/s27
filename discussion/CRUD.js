	/* s27-CRUD.js */


	const http = require("http");

//Mock Database
let database = [
	{
		"name":"Brandon",
		"email":"brandon@mail.com"
	},
	{
		"name":"Jobert",
		"email":"jobert@mail.com"
	}
]

http.createServer((req,res) => {
	//route for returning all items upon receiving a get req
	
	if(req.url === "/users" && req.method ==="GET"){
		res.writeHead(200,{"Content-Type":"_application/json"});
		
		//res.write() function is used to print what is inside the parameters as a response
		//Input has to be in a form of string that is why JSON.stringify is used.
		//the data that will be recvd by the users/client fr the server will be in a form of 
			//stringified JSON
		
		res.write(JSON.stringify(database));
		res.end();  // no parameters needed; response is our own databasa
	}





	if (req.url === "/users" && req.method === "POST") {     
		let requestBody = ""
		
		/*
		data stream - flow/sequence of data

			data step - data is recvd fr the client & is processed in the stream called "data" 
			where the code/stmts will be triggered

			end step = only runs after the rqst has completely benn sent once the data has 
			already been processed
		*/




		req.on("data", function(data) {   //req.on is event listener....
			requestBody += data
		})
		req.on("end", function() {
			
			console.log(typeof requestBody)   // requestBody--- select Body in Postman
			// the server needs an obj to arrange info in the db more efficiently that is
				//why we need JSON.parse
			requestBody = JSON.parse(requestBody)


			let newUser ={
				"name": requestBody.name,
				"email":requestBody.email
			}
			//adds the 
			database.push(newUser);  //db is an array, w/c is why we push into an array of obj ; new obj--use save
			console.log(database);

			res.writeHead(200,{"Content-Type":"_application/json"});
			// the client needs string data type for easier readability, that is why we shd use
				//JSON stringify
			res.write(JSON.stringify(newUser));
			res.end();
		})
	   
	}



}).listen(4000)

console.log("Server is running at localhost:4000")

// server to client ==>  stringify
// client to server ==>  parse


//========================================
// 2)Terminal
// marizzehaideesalanga@Marizzes-MacBook-Pro discussion % node CRUD.js
// Server is running at localhost:4000

// 3)Postman
// Add Request ==> Get Request
// GET
// http://localhost:4000/users
// SEND


//========================================
//========================================
// Terminal:
// marizzehaideesalanga@Marizzes-MacBook-Pro discussion % node CRUD.js
// Server is running at localhost:4000

// Postman:
// B170 -s27
// Post Request
// POST
// http://localhost:4000/users
// click Body, then select raw & JSON 
// Type inside body:(press on TAB before opening quote—name should be colored)
// {
//     "name":"john",
//     "email":"john@mail.com"
// }
// Press SEND

// Output:
// {
//     "name": "john",
//     "email": "john@mail.com"
// }

// TERMINAL output:
// marizzehaideesalanga@Marizzes-MacBook-Pro discussion % node CRUD.js
// Server is running at localhost:4000
// string
// [
//   { name: 'Brandon', email: 'brandon@mail.com' },
//   { name: 'Jobert', email: 'jobert@mail.com' },
//   { name: 'john', email: 'john@mail.com' }
// ]


