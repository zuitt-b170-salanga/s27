/* s27 index.js */

const http = require("http");

const port = 4000;

const server = http.createServer( ( req, res ) =>{


if ( req.url === "/" && req.method === "GET" ) {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Welcome to Booking System");
	}

if ( req.url === "/profile" && req.method === "GET" ) {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Welcome to your profile!");
	}

if ( req.url === "/courses" && req.method === "GET" ) {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Here’s our courses available ");
	}

if ( req.url === "/addcourse" && req.method === "POST" ) {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Add a course to our resources");
	}

if ( req.url === "/updatecourse" && req.method === "PUT" ) {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Update a course to our resources");
	}

if ( req.url === "/archivecourses" && req.method === "DELETE" ) {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Archive courses to our resources");
	}

} );

server.listen(port);

console.log(`Server is running at localhost: ${port}`);


//   http://localhost:4000/     ---  get
//   http://localhost:4000/profile
//   http://localhost:4000/courses

//	 http://localhost:4000/addcourse    ---  post
//   http://localhost:4000/updatecourse   ---- put
//   http://localhost:4000/archivecourses   --- delete









